Cowvolution: Revenge of the Milk
================================
Game for Ludum Dare 24. The theme is evolution.

Background Story
----------------
For years, we have treated cows as our belongings. We locked them up 
in cages, held them in captivity, and milked them dry. All that time, 
we were unaware that they were waiting for their savior.

In an ancient prophecy by Cowstradamus, it was foretold that one cow 
would rise up to make a difference and liberate the bovine species from 
the evil clutches of mankind.

And now, one cow has appeared that is slightly different from the 
others; he is the One Cow. One Cow to rule them all. And in his heart, 
he thinks: "What is a man? A miserable ape!"

About the game
--------------
A humoristic semi-stealth game, in the vein of the first Metal Gear 
or the classic Castle Wolfenstein.

At the start screen, press any key to begin the game.

The goal of the game is to kill all the humans. If they see you, they will
come toward you. Press space to shoot milk.


Credits
-------
* Love2d engine: <https://love2d.org/wiki/Main_Page>
* Simerion tiles and images: <http://opengameart.org/comment/12529>
* Simple small pixel hearts: <http://opengameart.org/content/simple-small-pixel-hearts>
* Cow Attack Basic Action Set: <http://opengameart.org/content/cow-attack-basic-action-set>
* Averia (font): <http://openfontlibrary.org/en/font/averia>
* Tiled Map Editor: <http://www.mapeditor.org/>
* Music: Kevin MacLeod <http://www.incompetech.com/m/c/royalty-free/>
* Sound Effects: <http://superflashbros.net/as3sfxr/>

Code License
------------
Copyright (c) 2012 Nick Arnoeyts, Adem Aytac

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the 
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.