-- Constants
Bullet = {}
local MILK_SPEED = 200
Bullet.default_speed = MILK_SPEED -- public static 

local function bullet_init_sprites(speed, bulletType, parent)
	-- make sure bulletType is not null
	assert(bulletType)
	local result = nil
	if bulletType == 'milk' then
		if speed > 0 then -- right
			result = parent.resource("drop-5x8-milk")
		elseif speed < 0 then -- left
			result = parent.resource("drop-5x8-milk")
		end
	end
	return result
end

-- Constructors
function Bullet.new(x, y, parent, speed, bulletType)
	-- optional arguments
	if not bulletType then bulletType = 'milk' end
	if not speed then speed = Bullet.default_speed end
	-- instance initialization
	obj = {}
	obj.parent = parent
	obj.type = "bullet"
	obj.x = x
	obj.y = y
	obj.speed = speed
	obj.image = bullet_init_sprites(speed, bulletType, parent)
	-- set metatable
	setmetatable(obj, {__index = Bullet} )
	return obj
end

-- Methods
function Bullet.update(self, delta)
	local speed = self.speed
	self.x = self.x + delta * speed
end

function Bullet.draw(self)
	love.graphics.draw(self.image, self.x, self.y)
end

-- Public Static Methods
function Bullet.reset()
	Bullet.default_speed = MILK_SPEED
end