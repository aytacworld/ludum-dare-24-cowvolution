require "Sprite"

-- Constants
Character = {}
local SPEED = 150

-- Private Methods
local function character_init_resources(parent)
	local result = Sprites.new()
	result:add("stand_l", parent.resource("cow-48x32-left"), '1,1', 48, 32)
	result:add("stand_r", parent.resource("cow-48x32-right"), '1,1', 48, 32)
	result:add("run_l", parent.resource("cow-48x32-left-step"), '1-2,1', 48, 32)
	result:add("run_r", parent.resource("cow-48x32-right-step"), '1-2,1', 48, 32)
	result:add("eat_l", parent.resource("cow-48x32-left-eat"), '1-2,1', 48, 32)
	result:add("eat_r", parent.resource("cow-48x32-right-eat"), '1-2,1', 48, 32)
	result:add("milk", parent.resource("drop-5x8-milk"), '1,1', 5, 8)
	return result
end

-- Constructors
function Character.new (x, y, parent)
	local obj = {}
	obj.parent = parent
	obj.type = "character"
	obj.x = x
	obj.y = y
	obj.width = 48
	obj.height = 32
	obj.direction = "left"
	obj.action = "stand"
	obj.spriteDirection = "left"
	obj.image = character_init_resources(parent)
	setmetatable(obj, {__index = Character} ) 
	return obj
end

-- Methods
function Character.run (self, direction, spriteDirection, delta)
	self.action = "run"
	local index = "_l"
	self.spriteDirection = spriteDirection
	if (spriteDirection == "right") then
		index = "_r"
	end
	index = self.action..index
	if (direction == "left") then
		self.x = self.x - SPEED * delta
	elseif (direction == "right") then
		self.x = self.x + SPEED * delta
	elseif (direction == "up") then
		self.y = self.y - SPEED * delta
	elseif (direction == "down") then
		self.y = self.y + SPEED * delta
	end
	self.image[index].Animation:update(delta)
end

function Character.attack(self)
	love.audio.play(self.parent.resource("shoot"))
	self.parent.make_bullet(self)
end

function Character.eat(self, direction, delta)
	self.spriteDirection = direction
	self.action = "eat"
	local index = "_l"
	if direction == right then
		index = "_r"
	end
	index = self.action..index
	self.image[index].Animation:update(delta)
end

-- Rendering
function Character.draw(self)
	local index = "_l"
	if self.spriteDirection == "right" then
		index = "_r"
	end
	index = self.action..index
	self.image[index].Animation:draw(self.image[index].Image, self.x, self.y)
	self.action = "stand"
end