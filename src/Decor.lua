Decor = {}

function Decor.new(x, y, image, solid)
	assert(image)
	if not solid then solid = false end
	-- object instantiation
	obj = {}
	obj.x = x
	obj.y = y
	obj.image = image
	obj.solid = solid
	setmetatable(obj, {__index = Decor} )
	return obj
end

function Decor.draw(self)
	love.graphics.draw(self.image, self.x, self.y)
end