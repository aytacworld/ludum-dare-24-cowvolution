require "Sprite"
Enemy = {}
local ENEMY_SPEED = 50
local ENEMY_WIDTH = 17
local ENEMY_HEIGHT = 37
Enemy.default_speed = ENEMY_SPEED -- public static

local function enemy_init_sprites(enemyType, parent, imageWidth, imageHeight)
	local result = Sprites.new()
	assert(enemyType)
	local standard_enemy = "cowboy"
	if enemyType == 'assassin' 
		or enemyType == 'cultist' 
		or enemyType == 'mrt' then
		standard_enemy = enemyType
	end
	result:add("left", parent.resource(standard_enemy.."_l"), '1,1', imageWidth, imageHeight)
	result:add("right", parent.resource(standard_enemy.."_r"), '1,1', imageWidth, imageHeight)
	return result
end

-- Constructors
function Enemy.new (source, target, parent, enemyType, speed)
	-- optional arguments
	if not enemyType then enemyType = 'cowboy' end
	if not speed then speed = Enemy.default_speed end
	-- instance initialization
	local obj = {}
	obj.parent = parent
	obj.type = "enemy"
	obj.width = ENEMY_WIDTH
	obj.height = ENEMY_HEIGHT
	obj.spriteDirection = "right"
	obj.heroInSight = false
	obj.speed = speed
	obj.target = "target"
	obj.targetRoute = target
	obj.sourceRoute = source
	obj.x = source.x
	obj.y = source.y
	obj.image = enemy_init_sprites(enemyType, parent, obj.width, obj.height)
	setmetatable(obj, {__index = Enemy} )
	return obj
end

-- Methods
function Enemy.update(self, delta)
	Enemy.run(self, delta)
	if (Enemy.look(self) == true) then
		self.heroInSight = true
	end
end

function Enemy.run (self, delta)
	-- Variables that doesn't change
	local tx = self.targetRoute.x
	local ty = self.targetRoute.y
	local sx = self.sourceRoute.x
	local sy = self.sourceRoute.y
	local sp = self.speed
	-- Variables that will change at the end
	local x = self.x
	local y = self.y
	local dir = self.spriteDirection
	local tar = self.target
	-- Temporary variables
	local changex = false
	local changey = false
	-- Swith targetRoute and sourceRoute values if target is not "target"
	if not (tar == "target") then
		tx = self.sourceRoute.x
		ty = self.sourceRoute.y
		sx = self.targetRoute.x
		sy = self.targetRoute.y
	end
	-- Move left or right, up or down
	if (self.heroInSight == true) then
		local hero = self.parent.hero
		x, changex, tx = Enemy.MoveRoute(x, hero.x, x, sp, delta)
		y, changey = Enemy.MoveRoute(y, hero.y, y, sp, delta)
	else
		x, changex, tx = Enemy.MoveRoute(sx, tx, x, sp, delta)
		y, changey = Enemy.MoveRoute(sy, ty, y, sp, delta)
	end
	-- Set sprite direction
	if x > tx then
		dir = "left"
	else
		dir = "right"
	end
	-- Change target if needed
	if (changex == true) and (changey == true) then
		if tar == "target" then
			tar = "source"
		else
			tar = "target"
		end
	end
	-- Change Enemy properties
	self.x = x
	self.y = y
	self.spriteDirection = dir
	self.target = tar
	-- Update Sprite
	self.image[dir].Animation:update(delta)
end

function Enemy.MoveRoute(source, target, current, speed, delta)
	local change = false
	if (source > target) then
		if (current >= target) then
			current = current - speed * delta
		else
			change = true
		end
	else
		if (current <= target) then
			current = current + speed * delta
		else
			change = true
		end
	end
	return current, change, target
end

function Enemy.look (self)
	local dir = self.spriteDirection
	local hero = self.parent.hero
	local hero_x = hero.x
	local hero_y = hero.y
	local hero_h = hero.y + hero.height
	local enemy_x = self.x
	local enemy_y = self.y
	local enemy_h = enemy_y + self.height
	if (dir == "left") then
		if (hero_x <= enemy_x) then
			if (enemy_y >= hero_y) and (enemy_y <= hero_h) or
			   (enemy_h >= hero_y) and (enemy_h <= hero_h) then
				return true
			end
		end
	else
		if (hero_x >= enemy_x) then
			if (enemy_y >= hero_y) and (enemy_y <= hero_h) or
			   (enemy_h >= hero_y) and (enemy_h <= hero_h) then
				return true
			end
		end
	end
	return false
end

function Enemy.draw(self)
	local index = self.spriteDirection
	self.image[index].Animation:draw(self.image[index].Image, self.x, self.y)
end