

GameOverScreen = {}

function GameOverScreen.init (resmgr)
	GameOverScreen.bg = resmgr.get("gameover")
	--GameOverScreen.time = 0
	GameOverScreen.bgm = resmgr.get("NetherworldShanty")
	GameOverScreen.bgm:setLooping(true)
end

function GameOverScreen.update (delta)
	--GameOverScreen.time = GameOverScreen.time + delta
end

function GameOverScreen.draw ()
	love.graphics.draw(GameOverScreen.bg, 0, 0)
end

function GameOverScreen.keypressed (key, unicode)
	love.event.push("quit")
end

function GameOverScreen.win()
	GameOverScreen.bg = resmgr.get("win")
	GameOverScreen.bgm = resmgr.get("BrightlyFancy")
	GameOverScreen.bgm:setLooping(true)
end