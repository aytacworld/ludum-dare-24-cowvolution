require "GameOverScreen"
require "Character"
require "Bullet"
require "Decor"
require "Enemy"
local bump = require "lib.bump"

-- GameState manager
-- Used as a parent for all game objects
-- Holds the resource manager and the state of all game objects
GameState = {}
local resmgr = nil

---------------------
-- Private Functions
-- initialize the enemies
local function gamestate_init_enemies()
	local enemies =  {
		Enemy.new({x=600,y=200}, {x=650,y=250}, GameState);
		Enemy.new({x=550,y=275}, {x=450,y=400}, GameState, 'mrt');
		Enemy.new({x=200,y=250}, {x=400,y=250}, GameState, 'assassin');
		Enemy.new({x=60,y=50}, {x=52,y=450}, GameState, 'cultist');
	}
	-- add enemies to collision detection
	for i, enemy in pairs(enemies) do
		bump.add(enemy)
	end
	return enemies
end

local function gamestate_init_decor ()
	return {
		Decor.new(620, 482, GameState.resource("grass") );
		Decor.new(699, 162, GameState.resource("hanger") );
		Decor.new(499, 12, GameState.resource("machine1") );
	}
end
------------------------------------------------------
-----BUMP
-- Check for collision
function bump.collision(item1, item2, dx, dy)
	if item1.type == 'bullet' and item2.type == 'enemy'
		or item2.type == 'bullet' and item1.type == 'enemy' then
			local bullets, enemies = GameState.bullets, GameState.enemies
			local bullet = item1.type == 'bullet' and item1 or item2
			local enemy = item1.type == 'enemy' and item1 or item2
			GameState.remove(bullet); bump.remove(bullet)
			GameState.remove(enemy); bump.remove(enemy)
	elseif item1.type == 'character' and item2.type == 'enemy'
		or item2.type == 'character' and item1.type == 'enemy' then
			GameState.bgm:stop()
			GameState.next = GameOverScreen
	end
end
function bump.getBBox(item)
	assert(item.type)
	local x, y, w, h = item.x, item.y, 0, 0
	if item.type == 'bullet' then 
		w, h = item.image:getWidth(), item.image:getHeight()
	else
		w, h = item.width, item.height
	end
	return x, y, w, h
end
--
function bump.shouldCollide(item1, item2)
  return true 
end
-----/BUMP
------------------------------------------------------
local function gamestate_check_bullet ()
	local remBullets = {}
	local bullets = GameState.bullets
	for i,bullet in ipairs(bullets) do
		if bullet.x < 0 or bullet.x > love.graphics.getWidth() then
			table.insert(remBullets, i)
		end
	end
	for i,v in ipairs(remBullets) do
		table.remove(bullets, v)
	end
end
-- get the background tile
local function make_map ()
	local tilemap = resmgr.get("map1")
	GameState.map = {
		image = tilemap,
		quad = love.graphics.newQuad(0, 0, love.graphics.getWidth(), love.graphics.getHeight(), tilemap:getWidth(), tilemap:getHeight())
	}
end
-- /Private Functions
---------------------

-- Initialize the GameState
function GameState.init(resourceManager)
	resmgr = resourceManager
	bump.initialize()
	GameState.bg = resmgr.get("map1")
	GameState.hero = Character.new(640, 525, GameState)
	bump.add(GameState.hero)
	GameState.bullets = {}
	GameState.enemies = gamestate_init_enemies()
	GameState.decor = gamestate_init_decor()
	GameState.bgm = resmgr.get("UmbrellaPants")
	GameState.bgm:setVolume(0.8)
	GameState.bgm:setLooping(true)
end

-- Update Function
function GameState.update(delta)
	-- If we've won, end the game
	if #(GameState.enemies) == 0 then
		GameState.bgm:stop()
		GameOverScreen.win()
		GameState.next = GameOverScreen
		return
	end
	-- Input Handling
	if love.keyboard.isDown("left") then
		GameState.hero:run("left", "left", delta) 
	elseif love.keyboard.isDown("right") then
		GameState.hero:run("right", "right", delta)
	elseif love.keyboard.isDown("up") then
		GameState.hero:run("up", GameState.hero.spriteDirection, delta)
	elseif love.keyboard.isDown("down") then
		GameState.hero:run("down", GameState.hero.spriteDirection, delta)
	elseif love.keyboard.isDown("e") then
		GameState.hero:eat(GameState.hero.spriteDirection, delta)
	end
	-- Update Game Objects
	for i, bullet in pairs(GameState.bullets) do
		bullet:update(delta)
	end
	-- Update Enemy
	for i, enemy in pairs(GameState.enemies) do
		enemy:update(delta)
	end	
	-- Execute Collision Detection
	bump.collide()
	gamestate_check_bullet()
end

-- Draw Function
function GameState.draw()
	-- draw background
	love.graphics.draw(GameState.bg, 0, 0)
	-- draw decoration
	for i, decoration in pairs(GameState.decor) do
		decoration:draw()
	end	
	-- draw hero
	GameState.hero:draw()
	-- draw bullets
	for i, bullet in pairs(GameState.bullets) do
		bullet:draw()
	end
	-- draw enemies
	for i, enemy in pairs(GameState.enemies) do
		enemy:draw()
	end
end

-- KeyPressed Function
function GameState.keypressed(key, unicode)
	if key == " " then
		GameState.hero:attack()
	end
end

-- Make a new bullet
function GameState.make_bullet(shooter)
	local x, y = shooter.x + 48 / 2, shooter.y + 32 / 2
	local bullet = Bullet.new(x, y, GameState)
	if shooter.spriteDirection == 'left' then bullet.speed = bullet.speed * -1 end
	table.insert(GameState.bullets, bullet)
	bump.add(bullet)
	return bullet
end

-- remove object from our GameState
function GameState.remove(o)
	local tab = nil
	if o.type == 'bullet' then tab = GameState.bullets
	elseif o.type == 'enemy' then tab = GameState.enemies
	end -- endif
	for i, v in pairs(tab) do
		if v == o then
			table.remove(tab, i)
			return
		end
	end
end

-- Request a resource
function GameState.resource(file)
	return resmgr.get(file)
end