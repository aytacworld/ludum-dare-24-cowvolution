HitPointBar = {}

function HitPointBar.new (maxHP, minHP, imageFull, imageEmpty)
	local obj = {}
	obj.maxHP = maxHP -- 1
	obj.minHP = minHP
	obj.currentHP = maxHP
	obj.imageFull = imageFull
	obj.imageEmpty = imageEmpty
	setmetatable(obj, {__index = HitPointBar} )
	return obj
end

function HitPointBar.getHP(self)
	return self.currentHP
end

function HitPointBar.setHP(self, value)
	self.currentHP = value
end

function HitPointBar.getMaxHP(self)
	return self.maxHP
end

function HitPointBar.setMaxHP(self, value)
	self.maxHP = value
end

function HitPointBar.increaseHP(self, value)
	local newHP = self.currentHP + value
	if (newHP < self.maxHP) then
		self.currentHP = newHP
	else
		self.currentHP = self.maxHP
	end
end

function HitPointBar.decreaseHP(self, value)
	local newHP = self.currentHP - value
	if (newHP > self.minHP) then
		self.currentHP = newHP
	else
		self.currentHP = self.minHP
	end
end

function  HitPointBar.draw(self)
	for i=self.minHP,self.maxHP-1 do
		local tempImg = nil
		if (i < self.currentHP) then
			tempImg = self.imageFull
		else
			tempImg = self.imageEmpty
		end
		love.graphics.draw(tempImg, 16 * i, 0)
	end
end