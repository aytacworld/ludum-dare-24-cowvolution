local anim8 = require "lib.anim8"
Sprites = {}

function Sprites.new ()
	local obj = {}
	obj.sprites = {}
	setmetatable(obj, {__index = Sprites} ) 
	return obj
end

function Sprites.add(self, id, img, frames, frameWidth, frameHeight)
	local anim = {}
	anim.Id = id
	anim.Image = img
	local grid = anim8.newGrid(frameWidth, frameHeight, img:getWidth(), img:getHeight())
	local ani = anim8.newAnimation('loop', grid(frames), 0.1)
	anim.Animation = ani
	self[id] = anim
end

function Sprites.get(self, id)
	local resource = self.sprites[id]
	if not resource then return nil end
	return resource
end