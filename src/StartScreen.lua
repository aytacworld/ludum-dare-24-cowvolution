require "GameState"

StartScreen = {}

function StartScreen.init (resmgr)
	StartScreen.bg = resmgr.get("title")
	--StartScreen.time = 0
	StartScreen.bgm = resmgr.get("Sneaky")
	StartScreen.bgm:setLooping(true)
	love.audio.play(StartScreen.bgm)
end

function StartScreen.update (delta)
	--StartScreen.time = StartScreen.time + delta
end

function StartScreen.draw ()
	love.graphics.draw(StartScreen.bg, 0, 0)
end

function StartScreen.keypressed (key, unicode)
	StartScreen.bgm:stop()
	StartScreen.next = GameState 
end