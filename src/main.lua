-- Load our other modules
resmgr = require "ResourceManager"
require "GameState"
require "StartScreen"

function love.load ()
	love.graphics.setCaption( "Cowvolution: Revenge of the Milk" )
	GameState.init (resmgr)
	StartScreen.init (resmgr)
	GameOverScreen.init (resmgr)
	state = StartScreen
end

function love.update (delta)
	if paused then return end
	if state.next then
		state = state.next
		if state.bgm then love.audio.play(state.bgm) end
	end
	state.update(delta)
end

function love.draw ()
	state.draw()
end

function love.keypressed (key, unicode)
	if paused then return end
	if key == "escape" then
		love.event.push("quit")
	else
		state.keypressed(key, unicode)
	end
end

function love.focus (f)
	paused = not f
end